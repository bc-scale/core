"""
Actual backend implementation
"""
 # noqa: E501
from openapi_server.models.inline_response200 import InlineResponse200  # noqa: E501
from openapi_server.models.inline_response400 import InlineResponse400  # noqa: E501
from openapi_server import util
from connexion import ProblemException

# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import sqlalchemy as db
from sqlalchemy import or_, and_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

# Tools
from datetime import datetime

# Error handling
from flask import abort


# Connection
from ..db_models.db_connection import *
# Models
from openapi_server.models.inline_response400 import InlineResponse400

# ORDER IMPORTANT
#--------------------------------------------------------------------------
from openapi_server.models.balance_item import BalanceItem

from openapi_server.models.value_item import ValueItem
from ..db_models.value_item_db import ValueItemDB

from openapi_server.models.transaction_detail import TransactionDetail
from ..db_models.transaction_detail_item_db import TransactionDetailItemDB

from openapi_server.models.category_item import CategoryItem
from ..db_models.category_item_db import CategoryItemDB

from openapi_server.models.grid_settings_item import GridSettingsItem
from ..db_models.grid_settings_item_db import GridSettingsItemDB

# This has to be last since other have foreign key requirements
from openapi_server.models.transaction_item import TransactionItem
from ..db_models.transaction_item_db import TransactionItemDB

from openapi_server.models.share_item import ShareItem
from ..db_models.share_item_db import ShareItemDB

from openapi_server.models.share_position import SharePosition
from ..db_models.share_position_item_db import SharePositionItemDB

from openapi_server.models.coin_item import CoinItem
from ..db_models.coin_item_db import CoinItemDB

from openapi_server.models.coin_position import CoinPosition
from ..db_models.coin_position_item_db import CoinPositionItemDB
# ----------------------------------------------------------------------------

# Error Codes
ERROR_INVALID_TRANSACTION_ID = 411
ERROR_INVALID_CATEGORY = 412
ERROR_INVALID_GRID_ITEM = 413
ERROR_INVALID_SHARE_TYPE = 414

# CONSTANTS
DEFAULT_CATEGORY_ICON = 'question-circle'
DEFAULT_CATEGORY_DESCRIPTION = "No description available."

# UTILITY
def setup_encryption_key(key):
    ValueItemDB.set_encryption_key(key)
    TransactionDetailItemDB.set_encryption_key(key)
    TransactionItemDB.set_encryption_key(key)

class DefaultController_interface:
   
    def categories_get(number=None, token_info=None):
        """categories_get

        Obtain a list of all user categories. # noqa: E501

        :param number: Number of maximal returned objects.
        :type number: int

        :rtype: List[CategoryItem]
        """
        user = token_info['uid']
        print(f"Get categories user:{user}")

        setup_encryption_key(token_info['encryption_key'])

        ret_val = []

        # Search for all user categories
        for session in db_connection.get_session():
            category_list = session.query(CategoryItemDB).filter(CategoryItemDB.user_id==token_info['uid']).all()
            if category_list is not None:
                for category in category_list:
                    ret_val.append(CategoryItem(id=category.id, icon=category.icon, name=category.name, description=category.description))
            else:
                print("Could not find any category") 

        ret_val.sort(key=lambda x: x.name, reverse=False)

        return ret_val
    
    def category_category_id_delete(category_id, token_info=None):
        # TODO Add implementation
        abort(ERROR_INVALID_CATEGORY, f"This function is not yet implemented.")

    def category_post(category_item, token_info=None):  # noqa: E501
        """category_post

        Add a new user category. # noqa: E501

        :param category_item: 
        :type category_item: dict | bytes

        :rtype: None
        """
        icon = category_item.icon
        name = category_item.name
        description = category_item.description

        if name is None:
             abort(ERROR_INVALID_CATEGORY, f"Category name has to be provided.")
        if icon is None:
            icon = DEFAULT_CATEGORY_ICON
        if description is None:
            description = DEFAULT_CATEGORY_DESCRIPTION


        category_item_db = CategoryItemDB(user_id=token_info['uid'], icon=icon, name=name, description=description)
      
        session_generator = db_connection.get_session()
        for session in session_generator:
            session.add_all([category_item_db])
            session.commit()

    def transaction_transaction_id_get(transaction_id, token_info=None):
        user = token_info['uid']
        print(f"Get transaction user:{user}")

        setup_encryption_key(token_info['encryption_key'])

        success = True
        ret_val = None

        # Search for all user categories
        for session in db_connection.get_session():
            item = session.query(TransactionItemDB).filter(TransactionItemDB.user_id==user).filter(TransactionItemDB.id==transaction_id).first()
            if item is not None:
                print("Found requested id...")
                value_item = item.value
                detail_item = item.detail
                category_item = item.category

                ret_category=CategoryItem(id=category_item.id, icon=category_item.icon, name=category_item.name, description=category_item.description)
                ret_detail=TransactionDetail(source=detail_item.source, destination=detail_item.destination, timestamp=detail_item.timestamp, comment=detail_item.comment)
                ret_value=ValueItem(value=value_item.value, currency=value_item.currency)
                ret_val = TransactionItem(id=item.id, name=item.name, category=ret_category, detail=ret_detail, value=ret_value, created_by=item.created_by, created_at=item.created_at)

                success = True
            else:
                success = False
               
        if not success:
             abort(ERROR_INVALID_TRANSACTION_ID, f"Request for transaction with transcation id {transaction_id} is invalid.")
        print(ret_val)
        return ret_val

    def transaction_transaction_id_delete(transaction_id, token_info=None):
        user = token_info['uid']
        print(f"Delete transaction user:{user}")

        if transaction_id is None:
            abort(ERROR_INVALID_TRANSACTION_ID, f"No transacion id provided.")

        setup_encryption_key(token_info['encryption_key'])

        success = True
        # Search
        ret_val = []

        session_generator = db_connection.get_session()
        for session in session_generator:
            item = session.query(TransactionItemDB).filter(TransactionItemDB.user_id==user).filter(TransactionItemDB.id==transaction_id).first()
            if item is not None:
                print(f"Deleting {item}...")
                detail = item.detail
                value = item.value
                session.delete(item)
                session.delete(value)
                session.delete(detail)
                session.commit()
            else:
                success = False
        if not success:
            abort(ERROR_INVALID_TRANSACTION_ID, f"Request for transaction with transcation id {transaction_id} is invalid.")
               
        
    def category_category_id_put(category_id, category_item, token_info=None):
        # TODO Add implementation
        abort(ERROR_INVALID_CATEGORY, f"This function is not yet implemented.")

    def transaction_transaction_id_put(transaction_id, transaction_item, token_info=None):
        user = token_info['uid']
        print(f"Put transaction user:{user}")

        if transaction_id is None:
            abort(ERROR_INVALID_TRANSACTION_ID, f"No transacion id provided.")

        setup_encryption_key(token_info['encryption_key'])

        success = True
        # Search
        ret_val = []

        session_generator = db_connection.get_session()
        for session in session_generator:
            item = session.query(TransactionItemDB).filter(TransactionItemDB.user_id==user).filter(TransactionItemDB.id==transaction_id).first()
            if item is not None:
                print(f"Updating: values for {item}...")

                # Update value
                item.value.value = transaction_item.value.value
                item.value.currency = transaction_item.value.currency
                # Update details
                item.detail.source = transaction_item.detail.source
                item.detail.destination = transaction_item.detail.destination
                item.detail.timestamp = transaction_item.detail.timestamp
                item.detail.comment = transaction_item.detail.comment

                # Resolve category
                if transaction_item.category.name is not None or transaction_item.category.id is not None:
                    # Try to find category
                    category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.user_id==token_info['uid']).filter(or_(CategoryItemDB.name==transaction_item.category.name, CategoryItemDB.id==transaction_item.category.id)).first()
                    if category_item_db is None:
                        abort(ERROR_INVALID_CATEGORY, f"Category {transaction_item.category.name} is unknown.")
                else:
                    # Try to find default category
                    category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.name=='Unspecified').first()
                    if category_item_db is None:
                        print("Created default category.")
                        # Create unspecified category
                        category_item_db = CategoryItemDB(user_id=token_info['uid'], icon=DEFAULT_CATEGORY_ICON, name="Unspecified", description='This is the default category for not specified transactions')


                print("Found:")
                print(category_item_db)
                # Update category
                item.category = category_item_db

                # Update Item
                item.name = transaction_item.name

                print(item)
                session.commit()

            else:
                success = False
               
        if not success:
             abort(ERROR_INVALID_TRANSACTION_ID, f"Request for transaction with transcation id {transaction_id} is invalid.")

    def transaction_post(transaction_item, token_info=None):
        user = token_info['uid']
        print(f"Post transaction user:{user}")

        setup_encryption_key(token_info['encryption_key'])

        category_item_db = None
        session_generator = db_connection.get_session()
        for session in session_generator:
            if transaction_item.category.name is not None or transaction_item.category.id is not None:
                # Try to find category
                category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.user_id==token_info['uid']).filter(or_(CategoryItemDB.name==transaction_item.category.name, CategoryItemDB.id==transaction_item.category.id)).first()
                if category_item_db is None:
                    abort(ERROR_INVALID_CATEGORY, f"Category {transaction_item.category.name} is unknown.")
            else:
                # Try to find default category
                category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.name=='Unspecified').first()
                if category_item_db is None:
                    print("Created default category.")
                    # Create unspecified category
                    category_item_db = CategoryItemDB(user_id=token_info['uid'], icon=DEFAULT_CATEGORY_ICON, name="Unspecified", description='This is the default category for not specified transactions')
        

            value_item_db = ValueItemDB(value=transaction_item.value.value, currency=transaction_item.value.currency)
            detail_item_db = TransactionDetailItemDB(source=transaction_item.detail.source, destination=transaction_item.detail.destination, timestamp=transaction_item.detail.timestamp, comment=transaction_item.detail.comment)
            transaction_item_db = TransactionItemDB(user_id=token_info['uid'], name=transaction_item.name, created_by="user", value_item=value_item_db, detail_item=detail_item_db, category_item=category_item_db)
            
            session.add_all([transaction_item_db])
            session.commit()
     
    def transactions_history_get(start_date, end_date, start_number, end_number, category, maximum_results, token_info=None):  # noqa: E501
        """transactions_history_get

        Obtain a list of all user transactions. # noqa: E501

        :param start_date: 
        :type start_date: str
        :param end_date: 
        :type end_date: str

        :rtype: List[TransactionItem]
        """
        # Ignore start_date, end_date for now
        user_id = token_info['uid']

        # Set encryption keys
        setup_encryption_key(token_info['encryption_key'])

        print(f"Get History: params: start_date:{start_date}, end_date:{end_date}, start_number:{start_number}, end_number:{end_number}, category:{category}, maximum_results:{maximum_results}")

        # Search
        ret_val = []

        session_generator = db_connection.get_session()
        for session in session_generator:
            response = session.query(TransactionItemDB).join(TransactionDetailItemDB).join(CategoryItemDB).filter(TransactionItemDB.user_id == user_id)
            if category is not None:
                response = response.filter(CategoryItemDB.name == category)

            if maximum_results is not None:
                response = response.limit(maximum_results)

            response = response.all()
            if response is not None:
                for item in response: 
                    value_item = item.value
                    detail_item = item.detail
                    category_item = item.category

                    if start_date is not None:
                        # Apply start_date filter
                        print(start_date)
                        print(detail_item.timestamp)
                        if detail_item.timestamp < start_date:
                            continue
                    if end_date is not None:
                        # Apply end_date filter
                        if detail_item.timestamp > end_date:
                            continue

                    ret_category=CategoryItem(id=category_item.id, icon=category_item.icon, name=category_item.name, description=category_item.description)
                    ret_detail=TransactionDetail(source=detail_item.source, destination=detail_item.destination, timestamp=detail_item.timestamp, comment=detail_item.comment)
                    ret_value=ValueItem(value=value_item.value, currency=value_item.currency)
                    ret_val.append(TransactionItem(id=item.id, name=item.name, category=ret_category, detail=ret_detail, value=ret_value, created_by=item.created_by, created_at=item.created_at))
            else:
                print("No item found for query")

        ret_val.sort(key=lambda x: x.detail.timestamp, reverse=False)

        return ret_val

    def transactions_count_get(category, token_info=None):
        user_id = token_info['uid']

        # Set encryption keys
        setup_encryption_key(token_info['encryption_key'])

        ret_val = None

        session_generator = db_connection.get_session()
        for session in session_generator:
            query = session.query(func.count(TransactionItemDB.id)).filter(TransactionItemDB.user_id == user_id)
            
            if category is not None:
                query = query.filter(CategoryItemDB.name == category)
            ret_val = query.scalar()
            print(ret_val)

        return {'transaction_count':ret_val}

    def balance_current_get(reserved, token_info=None):
        user_id = token_info['uid']

        # Set encryption keys
        setup_encryption_key(token_info['encryption_key'])

        balance_curr = 0

        session_generator = db_connection.get_session()
        for session in session_generator:
            query = session.query(TransactionItemDB).join(TransactionDetailItemDB).filter(TransactionItemDB.user_id == user_id).all()
            if query is not None:
                for item in query: 
                    value_item = item.value
                    balance_curr += value_item.value

        return {'balance':ValueItem(value=balance_curr, currency="€")}
        
    def grid_settings_get(item_id, token_info=None):
        user_id = token_info['uid']

        print("grid_settings_get")

        grid_items = []

        session_generator = db_connection.get_session()
        for session in session_generator:
            if item_id is not None:
                query = session.query(GridSettingsItemDB).filter(GridSettingsItemDB.user_id == user_id).filter(GridSettingsItemDB.grid_item_id == item_id).all()
                print(f"Return specific grid setting for {item_id}")
            else:
                query = session.query(GridSettingsItemDB).filter(GridSettingsItemDB.user_id == user_id).all()
                print("Return all available grid settings")
            if query is not None:
                for item in query:
                    grid_item = GridSettingsItem(grid_item_id=item.grid_item_id, grid_x_pos=item.grid_x_pos, grid_y_pos=item.grid_y_pos, grid_width=item.grid_width, grid_height=item.grid_height)
                    grid_items.append(grid_item)

        if grid_items == []:
            abort(ERROR_INVALID_GRID_ITEM, f"Request for transaction grid setting item {item_id} is invalid.")

        return grid_items

    
    def grid_settings_put(grid_settings_item, token_info=None):
        user_id = token_info['uid']
        print("grid_settings_put")
        
        session_generator = db_connection.get_session()
        for session in session_generator:
        
            grid_item = session.query(GridSettingsItemDB).filter(GridSettingsItemDB.grid_item_id==grid_settings_item.grid_item_id).first()
            if grid_item is None:
                print(f"Created new Setting for grid item {grid_settings_item.grid_item_id}.")
                # Create new UI element
                grid_item = GridSettingsItemDB(user_id=user_id, grid_item_id=grid_settings_item.grid_item_id, grid_x_pos=grid_settings_item.grid_x_pos, grid_y_pos=grid_settings_item.grid_y_pos, grid_width=grid_settings_item.grid_width, grid_height=grid_settings_item.grid_height)
                session.add_all([grid_item])
            else:
                print(f"Update {grid_item}")
                grid_item.grid_x_pos = grid_settings_item.grid_x_pos
                grid_item.grid_y_pos = grid_settings_item.grid_y_pos
                grid_item.grid_width = grid_settings_item.grid_width
                grid_item.grid_height = grid_settings_item.grid_height

            session.commit()

    def share_type_post(share_item, token_info=None):  # noqa: E501
        """share_type_post

        Add a new share type # noqa: E501

        :param share_item: 
        :type share_item: dict | bytes

        :rtype: None
        """
        user_id = token_info['uid']
        print("share_type_post")
        
        setup_encryption_key(token_info['encryption_key'])

        session_generator = db_connection.get_session()
        for session in session_generator:
            
            share_item = ShareItemDB(user_id=token_info['uid'], name=share_item.name, ticker=share_item.ticker, isin=share_item.isin)
            
            session.add_all([share_item])
            session.commit()


    def share_type_share_type_id_delete(share_type_id, token_info=None):  # noqa: E501
        """share_type_share_type_id_delete

        Delete an existing share type # noqa: E501

        :param share_type_id: share type that has to be deleted
        :type share_type_id: int

        :rtype: None
        """
        pass


    def share_types_get(name=None, token_info=None):  # noqa: E501
        """share_types_get

        Obtain a list of user share types. Don&#39;t use optional parameters to get all user share types # noqa: E501

        :param name: Specify a share type by its name.
        :type name: str

        :rtype: List[ShareItem]
        """
        user = token_info['uid']
        print(f"share_types_get: name {name}")

        setup_encryption_key(token_info['encryption_key'])
        ret_val = []


        # Search for all user categories
        for session in db_connection.get_session():
            if name is None:
                # Find all shares
                share_types = session.query(ShareItemDB).filter(ShareItemDB.user_id==token_info['uid']).all()
            else:
                # Find only specified share
                share_types = session.query(ShareItemDB).filter(ShareItemDB.user_id==token_info['uid']).filter(ShareItemDB.name==name).all()
          
            if share_types != []:
                print(share_types)
                for share_type in share_types:
                    ret_val.append(ShareItem(id=share_type.id, name=share_type.name, ticker=share_type.ticker, isin=share_type.isin))
            else:
                print("Could not find any share types") 

        ret_val.sort(key=lambda x: x.ticker, reverse=False)

        return ret_val

    def share_position_post(share_position=None, token_info=None):  # noqa: E501
        """share_position_post

        Open a new share position # noqa: E501

        :param share_position: 
        :type share_position: dict | bytes

        :rtype: None
        """
        user = token_info['uid']
        print(f"share_position_post")

        setup_encryption_key(token_info['encryption_key'])

        transaction_item_db = None
        category_item_db = None
        session_generator = db_connection.get_session()
        for session in session_generator:
            transaction_item = share_position.open_transaction
            if transaction_item.category.name is not None or transaction_item.category.id is not None:
                # Try to find category
                category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.user_id==token_info['uid']).filter(or_(CategoryItemDB.name==transaction_item.category.name, CategoryItemDB.id==transaction_item.category.id)).first()
                if category_item_db is None:
                    abort(ERROR_INVALID_CATEGORY, f"Category {transaction_item.category.name} is unknown.")
            else:
                # Try to find default category
                category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.name=='Unspecified').first()
                if category_item_db is None:
                    print("Created default category.")
                    # Create unspecified category
                    category_item_db = CategoryItemDB(user_id=token_info['uid'], icon=DEFAULT_CATEGORY_ICON, name="Unspecified", description='This is the default category for not specified transactions')
        

            value_item_db = ValueItemDB(value=transaction_item.value.value, currency=transaction_item.value.currency)
            detail_item_db = TransactionDetailItemDB(source=transaction_item.detail.source, destination=transaction_item.detail.destination, timestamp=transaction_item.detail.timestamp, comment=transaction_item.detail.comment)
            transaction_item_db = TransactionItemDB(user_id=token_info['uid'], name=transaction_item.name, created_by="user", value_item=value_item_db, detail_item=detail_item_db, category_item=category_item_db)
            
            session.add_all([transaction_item_db])
            session.commit()

        share_type = None
        # Find share type
        for session in db_connection.get_session():
            share_types = session.query(ShareItemDB).filter(ShareItemDB.user_id==token_info['uid'], ShareItemDB.name==share_position.share_type.name).all()
            if share_types is not None:
               share_type = share_types[0]
            else:
                abort(ERROR_INVALID_SHARE_TYPE, f"Share type {share_position.share_type.name} is unknown.")

     
        # Create position
        for session in db_connection.get_session():
            share_item = SharePositionItemDB(user_id=token_info['uid'],
            amount=share_position.amount,
            comment=share_position.comment,
            depot=share_position.depot,
            share_type=share_type,
            open_transaction=transaction_item_db,
            close_transactions=[])
            
            session.add_all([share_item])
            session.commit()

    def coin_position_get(number, token_info=None):
        user = token_info['uid']
        print(f"coin_position_get")
        setup_encryption_key(token_info['encryption_key'])

        ret_val = []

        # Search for all user categories
        for session in db_connection.get_session():
            # Find only specified share
            open_positions = session.query(CoinPositionItemDB)\
                .filter(CoinPositionItemDB.user_id==token_info['uid'])\
                .all()
          
            if open_positions is not None:
                for position in open_positions:

                    # Create transaction item
                    value_item = position.open_transaction.value
                    detail_item = position.open_transaction.detail
                    category_item = position.open_transaction.category

                    open_transcation_category=CategoryItem(id=category_item.id, icon=category_item.icon, name=category_item.name, description=category_item.description)
                    open_transaction_detail=TransactionDetail(source=detail_item.source, destination=detail_item.destination, timestamp=detail_item.timestamp, comment=detail_item.comment)
                    open_transaction_value=ValueItem(value=value_item.value, currency=value_item.currency)
                    open_transaction = TransactionItem(id=position.open_transaction.id, name=position.open_transaction.name, category=open_transcation_category, detail=open_transaction_detail, value=open_transaction_value, created_by=position.open_transaction.created_by, created_at=position.open_transaction.created_at)

                    # Create coin item
                    coin_type = CoinItem(id=position.coin_type.id, name=position.coin_type.name, ticker=position.coin_type.ticker)

                    # Create coin positon
                    coin_position = CoinPosition(id=position.id, amount=position.amount, coin_type=coin_type, open_transaction=open_transaction, close_transactions=[], comment=position.comment, exchange=position.exchange)

                    ret_val.append(coin_position)
            else:
                print("Could not find any open coin positions") 

        ret_val.sort(key=lambda x: x.amount, reverse=False)

        return ret_val

    def coin_position_post(coin_position, token_info=None):
        user = token_info['uid']
        print(f"coin_position_post")

        setup_encryption_key(token_info['encryption_key'])

        transaction_item_db = None
        category_item_db = None
        session_generator = db_connection.get_session()
        for session in session_generator:
            transaction_item = coin_position.open_transaction
            if transaction_item.category.name is not None or transaction_item.category.id is not None:
                # Try to find category
                category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.user_id==token_info['uid']).filter(or_(CategoryItemDB.name==transaction_item.category.name, CategoryItemDB.id==transaction_item.category.id)).first()
                if category_item_db is None:
                    abort(ERROR_INVALID_CATEGORY, f"Category {transaction_item.category.name} is unknown.")
            else:
                # Try to find default category
                category_item_db = session.query(CategoryItemDB).filter(CategoryItemDB.name=='Unspecified').first()
                if category_item_db is None:
                    print("Created default category.")
                    # Create unspecified category
                    category_item_db = CategoryItemDB(user_id=token_info['uid'], icon=DEFAULT_CATEGORY_ICON, name="Unspecified", description='This is the default category for not specified transactions')
        

            value_item_db = ValueItemDB(value=transaction_item.value.value, currency=transaction_item.value.currency)
            detail_item_db = TransactionDetailItemDB(source=transaction_item.detail.source, destination=transaction_item.detail.destination, timestamp=transaction_item.detail.timestamp, comment=transaction_item.detail.comment)
            transaction_item_db = TransactionItemDB(user_id=token_info['uid'], name=transaction_item.name, created_by="user", value_item=value_item_db, detail_item=detail_item_db, category_item=category_item_db)
            
            session.add_all([transaction_item_db])
            session.commit()

        coin_type = None
        # Find share type
        for session in db_connection.get_session():
            coin_types = session.query(CoinItemDB).filter(CoinItemDB.user_id==token_info['uid'], CoinItemDB.name==coin_position.coin_type.name).all()
            if coin_types is not None:
               coin_type = coin_types[0]
            else:
                abort(ERROR_INVALID_SHARE_TYPE, f"Coin type {coin_position.coin_type.name} is unknown.")

     
        # Create position
        for session in db_connection.get_session():
            share_item = CoinPositionItemDB(user_id=token_info['uid'],
            amount=coin_position.amount,
            comment=coin_position.comment,
            exchange=coin_position.exchange,
            coin_type=coin_type,
            open_transaction=transaction_item_db,
            close_transactions=[])
            
            session.add_all([share_item])
            session.commit()

    def share_position_get(number=None, token_info=None):  # noqa: E501
        """share_position_get

        Obtain a list of all open share positions # noqa: E501

        :param number: Maximum
        :type number: 

        :rtype: List[SharePosition]
        """
        user = token_info['uid']
        print(f"share_position_get")
        setup_encryption_key(token_info['encryption_key'])

        ret_val = []

        # Search for all user categories
        for session in db_connection.get_session():
            # Find only specified share
            open_positions = session.query(SharePositionItemDB)\
                .filter(SharePositionItemDB.user_id==token_info['uid'])\
                .all()
          
            if open_positions is not None:
                for position in open_positions:

                    # Create transaction item
                    value_item = position.open_transaction.value
                    detail_item = position.open_transaction.detail
                    category_item = position.open_transaction.category

                    open_transcation_category=CategoryItem(id=category_item.id, icon=category_item.icon, name=category_item.name, description=category_item.description)
                    open_transaction_detail=TransactionDetail(source=detail_item.source, destination=detail_item.destination, timestamp=detail_item.timestamp, comment=detail_item.comment)
                    open_transaction_value=ValueItem(value=value_item.value, currency=value_item.currency)
                    open_transaction = TransactionItem(id=position.open_transaction.id, name=position.open_transaction.name, category=open_transcation_category, detail=open_transaction_detail, value=open_transaction_value, created_by=position.open_transaction.created_by, created_at=position.open_transaction.created_at)

                    # Create share item
                    share_type = ShareItem(id=position.share_type.id, name=position.share_type.name, ticker=position.share_type.ticker, isin=position.share_type.isin)

                    # Create share positon
                    share_positon = SharePosition(id=position.id, amount=position.amount, share_type=share_type, open_transaction=open_transaction, close_transactions=[], comment=position.comment, depot=position.depot)

                    ret_val.append(share_positon)
            else:
                print("Could not find any open positions") 

        ret_val.sort(key=lambda x: x.amount, reverse=False)

        return ret_val

    def coin_type_post(coin_item, token_info=None):
        user_id = token_info['uid']
        print("coin_type_post")
        
        setup_encryption_key(token_info['encryption_key'])

        session_generator = db_connection.get_session()
        for session in session_generator:
            
            coin_item = CoinItemDB(user_id=token_info['uid'], ticker=coin_item.ticker, name=coin_item.name)
            
            session.add_all([coin_item])
            session.commit()

    def coin_types_get(name, token_info=None):
        user = token_info['uid']
        print(f"coin_types_get: name {name}")

        setup_encryption_key(token_info['encryption_key'])
        ret_val = []


        # Search for all user categories
        for session in db_connection.get_session():
            if name is None:
                # Find all coin types
                coin_types = session.query(CoinItemDB).filter(CoinItemDB.user_id==token_info['uid']).all()
            else:
                # Find only specified share
                coin_types = session.query(CoinItemDB).filter(CoinItemDB.user_id==token_info['uid']).filter(CoinItemDB.name==name).all()
          
            if coin_types != []:
                print(coin_types)
                for coin_type in coin_types:
                    ret_val.append(CoinItem(id=coin_type.id, name=coin_type.name, ticker=coin_type.ticker))
            else:
                print("Could not find any coin types") 

        ret_val.sort(key=lambda x: x.ticker, reverse=False)

        return ret_val

    def coin_type_coin_type_id_delete(coin_type_id, token_info=None):
        pass