# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os

from connexion import ProblemException

from functools import wraps
from sqlalchemy_utils.types.encrypted.padding import InvalidPaddingError

def use_session(method):
    @wraps(method)
    def _impl(self, *method_args, **method_kwargs):
        Session = sessionmaker(bind=self.engine)
        active_session = Session()
        ret_val = None
        try:
            ret_val = method(self, *method_args, **method_kwargs)
        except Exception as e:
            print(f"Error: {e}")
            active_session.rollback()
        active_session.close()
        
        return ret_val
    return _impl

class DBConnection:
    def __init__(self):
        username = os.environ['FINANCETRACKER_DB_USERNAME']
        password = os.environ['FINANCETRACKER_DB_PASSWORD']
        address = os.environ['FINANCETRACKER_DB_ADDRESS']
        if os.environ.get('LOCAL_DB_ADDRESS') is not None:
            # Test env
            connection_string = os.environ['FINANCETRACKER_DB_ADDRESS']
            self.engine = create_engine(connection_string, echo=False)
            self.base = declarative_base(bind=self.engine)
            self.session = sessionmaker(bind=self.engine)
        else:
            connection_string = 'mysql+mysqldb://' + username + ':' + password + '@' + address
            self.engine = create_engine(connection_string, echo=False, pool_size=20, max_overflow=100)
            self.base = declarative_base(bind=self.engine)
            self.session = sessionmaker(bind=self.engine)
       


    def get_session(self):
        ret_val = None
        active_session = self.session()
        try:
            yield active_session
        except InvalidPaddingError as e:
            print(f"Error: InvalidPaddingError. Decryption of query data of {db_class} failed.")
            active_session.rollback()
            raise ProblemException(401, 'Decryption Error', 'Failed to decrypt database with provided user credentials.')
        except Exception as e:
            print(f"Unspecified Database error during query of {db_class}")
            active_session.rollback()
            raise ProblemException(402, 'Database Error', 'Failed to read user data.')

        active_session.close()
        print("Closed connection.. (query)")      

    def add_data(self, data):
        active_session = self.session()
        try:
            active_session.add_all(data)
            active_session.commit()
        except Exception as e:
            print(f"Error: DB_Error: {e}")
            active_session.rollback()
        active_session.close()
        print("Closed connection.. (add_Data)")


# Create global DBConnection
db_connection = DBConnection()