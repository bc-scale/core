# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, DateTime, String, DECIMAL, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
import datetime

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *

class TransactionDetailItemDB(db_connection.base):
    __tablename__ = 'transaction_detail_item'
    secret_key = ''
    id = Column(Integer, primary_key=True)
    timestamp = Column(EncryptedType(DateTime,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    source = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    destination = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    comment = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))

    @staticmethod
    def set_encryption_key(secret_key):
        secret_key=secret_key
   
    def __init__(self, source="", destination="", timestamp=None, comment=""):
        if timestamp is None:
            self.timestamp = datetime.datetime.now()
        else:
            self.timestamp = timestamp
        self.source = source
        self.destination = destination
        self.comment = comment

db_connection.base.metadata.create_all() 