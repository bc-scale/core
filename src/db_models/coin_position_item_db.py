# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, ForeignKey, DateTime, DECIMAL
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *
import datetime

from sqlalchemy import Table

coin_position_association_table = Table('coin_position_association', db_connection.base.metadata,
    Column('coin_position_id', Integer, ForeignKey('coin_position.id')),
    Column('transaction_id', Integer, ForeignKey('transaction_item.id'))
)

class CoinPositionItemDB(db_connection.base):
    __tablename__ = 'coin_position'
    secret_key = ''
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    comment = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    exchange = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    wallet = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    amount = Column(EncryptedType(DECIMAL,
                                secret_key,
                                AesEngine,
                                'pkcs5'))

    coin_type_id =  Column(Integer, ForeignKey('coin_item.id'))
    coin_type = relationship("CoinItemDB")
    open_transaction_id =  Column(Integer, ForeignKey('transaction_item.id'))
    open_transaction = relationship("TransactionItemDB", foreign_keys=[open_transaction_id])
    close_transactions = relationship("TransactionItemDB",
        secondary=coin_position_association_table)

    @staticmethod
    def set_encryption_key(secret_key):
        secret_key=secret_key
   
    def __init__(self, user_id, amount, comment, exchange, coin_type, open_transaction, close_transactions=[]):
        self.user_id = user_id
        self.comment = comment
        self.amount = amount
        self.exchange = exchange
        self.coin_type = coin_type
        self.open_transaction = open_transaction
        self.close_transactions = close_transactions

    def __repr__(self):
        return f'<CoinPositionItemDB {self.id}, {self.user_id}, {self.comment}, {self.exchange}, {self.coin_type}, {self.open_transaction}, {self.close_transactions}>'


db_connection.base.metadata.create_all() 