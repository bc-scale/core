# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, ForeignKey, DateTime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *
import datetime


class TransactionItemDB(db_connection.base):
    __tablename__ = 'transaction_item'
    secret_key = ''
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    name = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    created_at = Column(EncryptedType(DateTime,
                                secret_key,
                                AesEngine,
                                'pkcs5')) 
    created_by = Column(EncryptedType(Unicode,
                            secret_key,
                            AesEngine,
                            'pkcs5')) 
    value_id =  Column(Integer, ForeignKey('value_item.id'))
    value = relationship("ValueItemDB", backref=backref("transaction_item", uselist=False))
    detail_id =  Column(Integer, ForeignKey('transaction_detail_item.id'))
    detail = relationship("TransactionDetailItemDB", backref=backref("transaction_item", uselist=False))
    category_id =  Column(Integer, ForeignKey('category_item.id'))
    category = relationship("CategoryItemDB")

    @staticmethod
    def set_encryption_key(secret_key):
        secret_key=secret_key
   
    def __init__(self, user_id, name, created_by, value_item, detail_item, category_item):
        self.user_id = user_id
        self.name = name
        self.value = value_item
        self.detail = detail_item
        self.category = category_item
        self.created_by = created_by
        self.created_at = datetime.datetime.now()

db_connection.base.metadata.create_all() 