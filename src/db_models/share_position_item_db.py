# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, ForeignKey, DateTime, DECIMAL
from sqlalchemy import create_engine
import sqlalchemy
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *
import datetime
from sqlalchemy import Table

share_position_association_table = Table('share_position_association', db_connection.base.metadata,
    Column('share_position_id', Integer, ForeignKey('share_position.id')),
    Column('transaction_id', Integer, ForeignKey('transaction_item.id'))
)

class SharePositionItemDB(db_connection.base):
    __tablename__ = 'share_position'
    secret_key = ''
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    comment = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    depot = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    amount = Column(EncryptedType(DECIMAL,
                                secret_key,
                                AesEngine,
                                'pkcs5'))

    share_type_id =  Column(Integer, ForeignKey('share_item.id'))
    share_type = relationship("ShareItemDB")
    open_transaction_id =  Column(Integer, ForeignKey('transaction_item.id'))
    open_transaction = relationship("TransactionItemDB", foreign_keys=[open_transaction_id])
    close_transactions = relationship("TransactionItemDB",
        secondary=share_position_association_table)

    @staticmethod
    def set_encryption_key(secret_key):
        secret_key=secret_key
   
    def __init__(self, user_id, amount, comment, depot, share_type, open_transaction, close_transactions=[]):
        self.user_id = user_id
        self.comment = comment
        self.amount = amount
        self.depot = depot
        self.share_type = share_type
        self.open_transaction = open_transaction
        self.close_transactions = close_transactions

    def __repr__(self):
        return f'<SharePositionItemDB {self.id}, {self.user_id}, {self.comment}, {self.depot}, {self.share_type}, {self.open_transaction}, {self.close_transactions}>'


db_connection.base.metadata.create_all() 