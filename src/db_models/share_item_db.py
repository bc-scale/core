# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DECIMAL, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *


class ShareItemDB(db_connection.base):
    __tablename__ = 'share_item'
    secret_key = ''
    user_id = Column(Integer)
    id = Column(Integer, primary_key=True)
    ticker = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    isin = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    name = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    
    @staticmethod
    def set_encryption_key(secret_key):
        secret_key=secret_key

    def __init__(self, user_id, ticker, name, isin):
        self.user_id = user_id
        self.ticker = ticker
        self.name = name
        self.isin = isin

    def __repr__(self):
        return f'<ShareItemDB {self.id}, {self.user_id}, {self.ticker}, {self.name}, {self.isin}>'

db_connection.base.metadata.create_all() 