# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, ForeignKey, DateTime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *
import datetime


class CategoryItemDB(db_connection.base):
    __tablename__ = 'category_item'
    secret_key = ''
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    icon = Column(Unicode(150))
    name =  Column(Unicode(255))
    description = Column(Unicode(800))

  
    def __init__(self, user_id, icon, name, description):
        self.user_id = user_id
        self.icon = icon
        self.name = name
        self.description = description

db_connection.base.metadata.create_all() 