# coding: utf-8

from __future__ import absolute_import
import unittest
import pytest
import locale
from flask import json
from six import BytesIO
import os
import requests

from ..server_interface.controllers_interface import *
from openapi_server.models.inline_response200 import InlineResponse200  # noqa: E501
from openapi_server.models.inline_response400 import InlineResponse400  # noqa: E501
from openapi_server.models.transaction_item import TransactionItem  # noqa: E501
from openapi_server.test import BaseTestCase
import time
from pytest_httpserver import HTTPServer


AUTH_SERVER = '/' + os.environ['FINANCETRACKER_OAUTH_SERVER_ROOT_URI']

@pytest.fixture(scope="session")
def dummy_server():
    httpserver = HTTPServer()
    httpserver.start()

    httpserver.expect_request(AUTH_SERVER + "api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
    httpserver.expect_request(AUTH_SERVER + "api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
    os.environ["FINANCETRACKER_OAUTH_SERVER_ROOT_URI"] = httpserver.url_for(AUTH_SERVER + "api/token_information")
    assert requests.get(httpserver.url_for(AUTH_SERVER + "api/token_information")).json() == {'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""}

    yield httpserver

    httpserver.stop()


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""


    def test_balance_current_get_db_empty(self):
        """Test case for balance_current_get

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            query_string = [('reserved', 'reserved_example')]
            headers = { 
                'Accept': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/balance/current',
                method='GET',
                headers=headers,
                query_string=query_string)
            # Check response
            self.assert200(response,
                        'Response body is : ' + response.data.decode('utf-8'))
            assert '"value": 0' in response.data.decode('utf-8')
            
    def test_categories_get(self):
        """Test case for categories_get

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            query_string = [('number', 56)]
            headers = { 
                'Accept': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/categories',
                method='GET',
                headers=headers,
                query_string=query_string)
            self.assert200(response,
                        'Response body is : ' + response.data.decode('utf-8'))

    def test_category_category_id_delete_invalid(self):
        """Test case for category_category_id_delete

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            headers = { 
                'Accept': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/category/{category_id}'.format(category_id=56),
                method='DELETE',
                headers=headers)
            assert str(ERROR_INVALID_CATEGORY) in response.data.decode('utf-8')
        
    def test_category_category_id_put_invalid(self):
        """Test case for category_category_id_put

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            category_item = {
                "icon" : "icon",
                "name" : "name",
                "description" : "description",
                "id" : 0
            }
            headers = { 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/category/{category_id}'.format(category_id=56),
                method='PUT',
                headers=headers,
                data=json.dumps(category_item),
                content_type='application/json')

            assert str(ERROR_INVALID_CATEGORY) in response.data.decode('utf-8')

    def test_category_post(self):
        """Test case for category_post

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            category_item = {
                "icon" : "icon",
                "name" : "name",
                "description" : "description",
                "id" : 0
            }
            headers = { 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/category',
                method='POST',
                headers=headers,
                data=json.dumps(category_item),
                content_type='application/json')

            assert response.data.decode('utf-8') == ""


    def test_transaction_post(self):
        """Test case for transaction_post

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            transaction_item = {
                "name" : "name",
                "created_at" : "2000-01-23T04:56:07.000+00:00",
                "id" : 0,
                "detail" : {
                    "destination" : "destination",
                    "comment" : "comment",
                    "source" : "source",
                    "timestamp" : "2000-01-23T04:56:07.000+00:00"
                },
                "category" : {
                    "icon" : "icon",
                    "name" : "name",
                    "description" : "description",
                    "id" : 0
                },
                "created_by" : "created_by",
                "value" : {
                    "currency" : "€",
                    "value" : 0.8008281904610115
                }
            }
            headers = { 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/transaction',
                method='POST',
                headers=headers,
                data=json.dumps(transaction_item),
                content_type='application/json')

            assert((str(ERROR_INVALID_CATEGORY) in response.data.decode('utf-8')) or (response.data.decode('utf-8') == ""))

    def test_transaction_transaction_id_delete(self):
        """Test case for transaction_transaction_id_delete

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            headers = { 
                'Accept': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/transaction/{transaction_id}'.format(transaction_id=56),
                method='DELETE',
                headers=headers)
            assert str(ERROR_INVALID_TRANSACTION_ID) in response.data.decode('utf-8')

    def test_transaction_transaction_id_get(self):
        """Test case for transaction_transaction_id_get

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            headers = { 
                'Accept': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/transaction/{transaction_id}'.format(transaction_id=56),
                method='GET',
                headers=headers)
            assert str(ERROR_INVALID_TRANSACTION_ID) in response.data.decode('utf-8')        
            

    def test_transaction_transaction_id_put_invalid(self):
        """Test case for transaction_transaction_id_put

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            transaction_item = {
                "name" : "name",
                "created_at" : "2000-01-23T04:56:07.000+00:00",
                "id" : 0,
                "detail" : {
                    "destination" : "destination",
                    "comment" : "comment",
                    "source" : "source",
                    "timestamp" : "2000-01-23T04:56:07.000+00:00"
                },
                "category" : {
                    "icon" : "icon",
                    "name" : "name",
                    "description" : "description",
                    "id" : 0
                },
                "created_by" : "created_by",
                "value" : {
                    "currency" : "€",
                    "value" : 0.8008281904610115
                }
            }
            headers = { 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/transaction/{transaction_id}'.format(transaction_id=56),
                method='PUT',
                headers=headers,
                data=json.dumps(transaction_item),
                content_type='application/json')

            assert str(ERROR_INVALID_TRANSACTION_ID) in response.data.decode('utf-8')      

    def test_transactions_count_get(self):
        """Test case for transactions_count_get

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            query_string = [('category', 'category_example')]
            headers = { 
                'Accept': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/transactions/count',
                method='GET',
                headers=headers,
                query_string=query_string)
            self.assert200(response,
                        'Response body is : ' + response.data.decode('utf-8'))

    def test_transactions_history_get(self):
        """Test case for transactions_history_get

        
        """
        with HTTPServer(port=9999) as httpserver:
            httpserver.expect_request("/api/token_information").respond_with_json({'error':"success", "error_description":"", "token_scope":"write_transactions read_transactions", "token_type":"", "client_id":"ClientID", "user_id":"1", "encryption_key":"key", "user_admin_rights":"", "admin_rights":""})
            httpserver.expect_request("/api/user_information").respond_with_json({'error':"success", "error_description":"", "username":"username", "first_name":"first_name", "last_name":"last_name", "email":"email@tester.de","user_admin_rights":"", "admin_rights":""})
           
            query_string = [('start_date', '2013-10-20T19:20:30+01:00'),
                            ('end_date', '2013-10-20T19:20:30+01:00'),
                            ('start_number', 2),
                            ('end_number', 2),
                            ('category', 'category_example'),
                            ('maximum_results', 2)]
            headers = { 
                'Accept': 'application/json',
                'Authorization': 'Bearer special-key',
            }
            response = self.client.open(
                '/financetracker/core/transactions/history',
                method='GET',
                headers=headers,
                query_string=query_string)
            self.assert200(response,
                        'Response body is : ' + response.data.decode('utf-8'))
 
if __name__ == '__main__':
    unittest.main()
